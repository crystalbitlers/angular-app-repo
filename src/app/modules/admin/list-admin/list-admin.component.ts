import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { UserService } from '../../user/user.service';
import { AdminService } from '../admin.service';
import { AppAuthService } from 'src/app/core/auth/auth.service';
import { DynamicScriptLoaderService } from 'src/app/shared/services/dynamic-script-loader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-admin',
  templateUrl: './list-admin.component.html',
  styleUrls: ['./list-admin.component.scss']
})
export class ListAdminComponent implements OnInit {

  users:[User]
  selectedUser:User
  isLoading:boolean=true;
  constructor(
     private router: Router,
     private adminService:AdminService,
     private dynamicScrLoader:DynamicScriptLoaderService
     ) { }

  ngOnInit() {
    this.adminService.getAdminUsers().subscribe(resp=>{
      if(resp && resp.success){
        this.users = resp.success.Data;
        this.isLoading =  false;
        this.dynamicScrLoader.loadSingle('data-table');   
        this.dynamicScrLoader.loadSingle('trigger-data-table');    
      }
    })
  }

  viewUserDetail(userIndex){
    this.selectedUser = this.users[userIndex]
    this.router.navigate(['/admin/update-admin'], {state: {data: this.selectedUser} } );
  }


}
