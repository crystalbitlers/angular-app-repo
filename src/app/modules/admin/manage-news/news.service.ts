import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { News } from './news';
import { HttpService } from 'src/app/core/http/httpservice.service';

  @Injectable({
      providedIn: 'root'
  })

  export class NewsService {


    constructor(private httpService:HttpService){
    }

    createNews(news:News):Observable<any>{   
      var payload = {title: news.title,body: news.body};
      console.log(payload)
      console.log(JSON.stringify(payload))
      return this.httpService.postRequest(`news_feed/create`,payload,true);
    }

    updateNews(news:News):Observable<any>{    
      return this.httpService.postRequest(`news_feed/update`,{"news_id":news.id,"title": news.title ,"body": news.body},true,null);
    }

    getNews():Observable<any>{
      return this.httpService.postRequest(`news_feed/list`,null);
    }


  }