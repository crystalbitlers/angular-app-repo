import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/shared/models/Category';
import { NewsService } from './news.service';
import { News } from './news';
import { DynamicScriptLoaderService } from 'src/app/shared/services/dynamic-script-loader.service';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  buttonText='Create News'
  updateButtonText='update'
  inEditMode=false;
  isLoading:boolean=true;
  news:News={};
  newsList=[]
  constructor(private newsServie:NewsService,private dynamicScrLoader:DynamicScriptLoaderService) { 
    this.getNews()
     
  }

  ngOnInit() {
  }

  addNews(news){
    if(news.body){
        this.buttonText = 'Submitting'
        this.newsServie.createNews(news).subscribe(resp=>{
          if(resp && resp.success){
            // console.log("got here :: "+JSON.stringify(news))
            this.getNews();
          }
          this.buttonText = 'Create News'
        })
    }    
  }

  updateNews(news:News){
    this.updateButtonText = 'updating';
    this.newsServie.updateNews(news).subscribe(resp=>{
      if(resp && resp.success){
        news.inEditMode = !news.inEditMode;
        // alert(resp.success.Message);
        this.updateButtonText = 'update'
      }
    })
  }

  deleteNews(category){
    // if(confirm(`Confirm Deletion of categoory ${category.category_name}`)){
    //   this.investmentService.deleteCategory(category).subscribe(resp=>{
    //     if(resp && resp.success){
    //       // alert(resp.success.Message);
    //       this.getNews()
    //     }
    //   })
    // }
  }

  getNews(){
    this.isLoading=true;
    this.newsServie.getNews().subscribe(resp=>{
      
      if(resp && resp.success){
        
        this.newsList = resp.success.Data;
        this.dynamicScrLoader.loadSingle('data-table');   
        this.dynamicScrLoader.loadSingle('trigger-data-table'); 
      }
      this.isLoading=false;
    })
  }

}
