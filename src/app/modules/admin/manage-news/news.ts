import { BasicModel } from 'src/app/shared/models/BasicModel';

export class News extends BasicModel{
    title?: string;
    body?: string;
    inEditMode?:boolean=false
}