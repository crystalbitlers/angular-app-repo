import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/shared/models/user';
import { CloudinaryService } from 'src/app/shared/services/cloudinary.service';
import { AdminService } from '../admin.service';
import { Admin } from 'src/app/shared/models/Admin';

@Component({
  selector: 'app-create-admin',
  templateUrl: './create-admin.component.html',
  styleUrls: ['./create-admin.component.scss']
})
export class CreateAdminComponent implements OnInit {

  @Input() public user:User={email:'',password:'',country:'',first_name:'',last_name:'',bank_name:''};
  @Input() public editable:boolean;
  isSubmitting;
  isLoading:boolean=true;
  countries:string[]=['Nigeria','Ghana']
  bankList:any=[]
  dateModel:Date;
  opt1selected:boolean=false;
  opt2selected:boolean=false;
  image;
  dayComponent = ['1','2','3','4','5','6','7','8','9','10',
                  '11','12','13','14','15','16','17','18','19','20',
                  '21','22','23','24','25','26','27','28','29','30','31'];
  monthComponent = [{count:'1',title:'Jan'},{count:'2',title:'Feb'},
  {count:'3',title:'Mar'},{count:'4',title:'Apr'},
  {count:'5',title:'May'},{count:'6',title:'Jun'},
  {count:'7',title:'Jul'},{count:'8',title:'Aug'},
  {count:'9',title:'Jan'},{count:'10',title:'Oct'},
  {count:'11',title:'Nov'},{count:'12',title:'Dec'}]


  constructor(private adminService:AdminService,
    private cloudinaryService:CloudinaryService) { 
    //  this.getBankList();
    }

  ngOnInit(){
      this.isLoading = false;
  }

  createProfile(){
      console.log(JSON.stringify(this.user))
      let adminDetail = new Admin();
      adminDetail.first_name = this.user.first_name;
      adminDetail.last_name = this.user.last_name;
      adminDetail.authentication_type ="E";
      adminDetail.password = "password";
      adminDetail.user_category = "Admin";
      adminDetail.email = this.user.email;

      this.isSubmitting = this.adminService.createAdminUser(adminDetail).subscribe(resp=>{
        if(resp && resp.success){
          alert(resp.success.Message)
        }
      });
  }


}
